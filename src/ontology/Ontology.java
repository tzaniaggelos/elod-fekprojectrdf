package ontology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * @author A. Tzanis
 */
public class Ontology {

	/** prefixes **/
	public static final String instancePrefix;
	public static final String eLodPrefix;
	public static final String foafPrefix;
	public static final String skosPrefix;
	public static final String orgPrefix;
	public static final String goodRelationsPrefix;	
	public static final String regOrgPrefix;
	public static final String dctermsPrefix;
	public static final String rdfsPrefix;
	
	/** Classes **/
	// eLod
	public static final Resource corporateAnnouncementResource; 
	public static final Resource organizationCategoryResource;
	public static final Resource fekResource;
	public static final Resource announcementSubjectResource;
	public static final Resource announcementSubjectSchemeResource;
	public static final Resource fekTypeResource;
	// SKOS
	public static final Resource conceptResource;
	public static final Resource conceptSchemeResource;
	// GR
	public static final Resource businessEntityResource;
	// FOAF
	public static final Resource agentResource;
	public static final Resource organizationResource;
	// org
	public static final Resource orgOrganizationResource;
	// rov
	public static final Resource registeredOrganizationResource;
		
	/** Object Properties **/
	// eLod
	public static final Property relatedFek; 			
	public static final Property fekType; 
	public static final Property isAnnouncementOf;		
	// rov
	public static final Property orgType;				 
	//SKOS
	public static final Property hasTopConcept;
	public static final Property inScheme;
	public static final Property topConceptOf;
	// dcterms
	public static final Property subject;				 
	
	/** Data Properties **/
	// eLod
	public static final Property maeNumber;				 
	public static final Property gemhNumber;			 
	public static final Property etUrl;					 
	public static final Property fekName;				 
	public static final Property fekYear;			 
	public static final Property fekNumber;			 
	public static final Property documentUrl;			 
	// SKOS
	public static final Property prefLabel;
	// dcterms	
	public static final Property modified;				 
	public static final Property dateAccepted;			 
	public static final Property issued;				 
	
	static {
		/** prefixes **/
		instancePrefix = "http://linkedeconomy.org/resource/";
		eLodPrefix = "http://linkedeconomy.org/ontology#";
		dctermsPrefix = "http://purl.org/dc/terms#";
		foafPrefix = "http://xmlns.com/foaf/0.1/";
		skosPrefix = "http://www.w3.org/2004/02/skos/core#";
		orgPrefix = "http://www.w3.org/ns/org#";
		goodRelationsPrefix = "http://purl.org/goodrelations/v1#";
		regOrgPrefix = "http://www.w3.org/ns/regorg#";
		rdfsPrefix = "http://www.w3.org/2000/01/rdf-schema#";
		
		/** Resources **/
		
		// eLod
		corporateAnnouncementResource = ResourceFactory.createResource(eLodPrefix + "CorporateAnnouncement"); 
		organizationCategoryResource = ResourceFactory.createResource(eLodPrefix + "OrganizationCategory");
		fekResource = ResourceFactory.createResource(eLodPrefix + "Fek");
		fekTypeResource = ResourceFactory.createResource(eLodPrefix + "FekType");
		announcementSubjectResource = ResourceFactory.createResource(eLodPrefix + "AnnouncementSubject");
		announcementSubjectSchemeResource = ResourceFactory.createResource(eLodPrefix + "AnnouncementSubjectScheme");
		// FOAF
		agentResource = ResourceFactory.createResource(foafPrefix + "Agent");
		organizationResource = ResourceFactory.createResource(foafPrefix + "Organization");
		// SKOS
		conceptResource = ResourceFactory.createResource(skosPrefix + "Concept");
		conceptSchemeResource = ResourceFactory.createResource(skosPrefix + "ConceptScheme");
		// GR
		businessEntityResource = ResourceFactory.createResource(goodRelationsPrefix + "BusinessEntity");
		// RegOrg
		registeredOrganizationResource = ResourceFactory.createResource(regOrgPrefix + "RegisteredOrganization");
		// org
		orgOrganizationResource = ResourceFactory.createResource(orgPrefix + "Organization");
				
		/** Object Properties **/
		// eLod
		relatedFek = ResourceFactory.createProperty(eLodPrefix + "relatedFek");		
		fekType = ResourceFactory.createProperty(eLodPrefix + "fekType");
		isAnnouncementOf = ResourceFactory.createProperty(eLodPrefix + "isAnnouncementOf");
		// rov
		orgType = ResourceFactory.createProperty(regOrgPrefix + "orgType");
		//SKOS
		hasTopConcept = ResourceFactory.createProperty(skosPrefix + "hasTopConcept");
		inScheme = ResourceFactory.createProperty(skosPrefix + "inScheme");
		topConceptOf = ResourceFactory.createProperty(skosPrefix + "topConceptOf");
		// dcterms
		subject = ResourceFactory.createProperty(dctermsPrefix + "subject");
		
		/** Data Properties **/
		// eLod
		maeNumber = ResourceFactory.createProperty(eLodPrefix + "maeNumber");
		gemhNumber = ResourceFactory.createProperty(eLodPrefix + "gemhNumber");
		etUrl = ResourceFactory.createProperty(eLodPrefix + "etUrl");
		fekName = ResourceFactory.createProperty(eLodPrefix + "fekName");
		fekYear = ResourceFactory.createProperty(eLodPrefix + "fekYear");
		fekNumber = ResourceFactory.createProperty(eLodPrefix + "fekNumber");
		documentUrl = ResourceFactory.createProperty(eLodPrefix + "documentUrl");
		//SKOS
		prefLabel = ResourceFactory.createProperty(skosPrefix + "prefLabel");
		// dcterms
		issued = ResourceFactory.createProperty(dctermsPrefix + "issued");
		dateAccepted = ResourceFactory.createProperty(dctermsPrefix + "dateAccepted");
		modified = ResourceFactory.createProperty(dctermsPrefix + "modified");
		
	}
}