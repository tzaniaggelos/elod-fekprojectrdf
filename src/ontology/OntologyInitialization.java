package ontology;

import com.hp.hpl.jena.rdf.model.Model;
//import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author A. Tzani
 */

public class OntologyInitialization {
	
/**
  * Add the necessary prefixes to the model we are currently 
  * working with.
  * 
  * @param Model the model1 we are currently working with
  * @param Model the model2 we are currently working with
  */
	public void setPrefixes(Model model1, Model model2) {
		
		/** Model1 **/
    	if (model1 != null){
    		model1.setNsPrefix("elod", Ontology.eLodPrefix);
    		model1.setNsPrefix("skos", Ontology.skosPrefix);
    		model1.setNsPrefix("gr", Ontology.goodRelationsPrefix);
    		model1.setNsPrefix("rov", Ontology.regOrgPrefix);
    		model1.setNsPrefix("org", Ontology.orgPrefix);
    		model1.setNsPrefix("foaf", Ontology.foafPrefix);
    		model1.setNsPrefix("dcterms", Ontology.dctermsPrefix);
		}
		
    	/** Model2 **/
    	if (model2 != null){
    		model2.setNsPrefix("elod", Ontology.eLodPrefix);
    		model2.setNsPrefix("dcterms", Ontology.dctermsPrefix);
    		model2.setNsPrefix("skos", Ontology.skosPrefix);
		}
	}
	
	/**
	 * Create the basic hierarchies of the OWL classes and their labels 
	 * to the model we are currently working with.
	 * 
	 * @param Model the model1 we are currently working with
	 * @param Model the model2 we are currently working with
	 */
	public void createHierarchies(Model model1, Model model2) {
		
		/** Model1 **/
		
		if (model1 != null){
			
			//Agent
			model1.add(Ontology.agentResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.agentResource, RDFS.label, model1.createLiteral("Agent", "en"));
			model1.add(Ontology.agentResource, RDFS.label, model1.createLiteral("���������", "el"));
			
			//Agent - Organization (FOAF)
			model1.add(Ontology.organizationResource, RDFS.subClassOf, Ontology.agentResource);
			model1.add(Ontology.organizationResource, RDFS.label, model1.createLiteral("Organization", "en"));
			model1.add(Ontology.organizationResource, RDFS.label, model1.createLiteral("����������", "el"));
		
			//Agent - BusinessEntity
			model1.add(Ontology.businessEntityResource, RDFS.subClassOf, Ontology.agentResource);
			model1.add(Ontology.businessEntityResource, RDFS.label, model1.createLiteral("Business Entity", "en"));
			model1.add(Ontology.businessEntityResource, RDFS.label, model1.createLiteral("�������������� ��������", "el"));
		
			//Agent - Organization (org)
			model1.add(Ontology.orgOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
			model1.add(Ontology.orgOrganizationResource, RDFS.label, model1.createLiteral("Organization", "en"));
			model1.add(Ontology.orgOrganizationResource, RDFS.label, model1.createLiteral("����������", "el"));
		
			//Agent - Registered Organization
			model1.add(Ontology.registeredOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
			model1.add(Ontology.registeredOrganizationResource, RDFS.label, model1.createLiteral("Registered Organization", "en"));
			model1.add(Ontology.registeredOrganizationResource, RDFS.label, model1.createLiteral("������������� ����������", "el"));
		
			//Concept
			model1.add(Ontology.conceptResource, RDFS.subClassOf, OWL.Thing);
		
			//Concept - Organization Category
			model1.add(Ontology.organizationCategoryResource, RDFS.subClassOf, Ontology.conceptResource);
			model1.add(Ontology.organizationCategoryResource, RDFS.label, model1.createLiteral("Organization Category", "en"));
			model1.add(Ontology.organizationCategoryResource, RDFS.label, model1.createLiteral("��������� ����������", "el"));
		}
						
		/** Model2 **/
		
		if (model2 != null){
	    	
			//Corporate Announcement															
			model2.add(Ontology.corporateAnnouncementResource, RDFS.subClassOf, OWL.Thing);		
			model2.add(Ontology.corporateAnnouncementResource, RDFS.label, model2.createLiteral("Corporate Announcement", "en"));
			model2.add(Ontology.corporateAnnouncementResource, RDFS.label, model2.createLiteral("�������� ����������", "el"));																		
		
			//Corporate Announcement - Fek														
			model2.add(Ontology.fekResource, RDFS.subClassOf, OWL.Thing);		
			model2.add(Ontology.fekResource, RDFS.label, model2.createLiteral("Fek", "en"));
			model2.add(Ontology.fekResource, RDFS.label, model2.createLiteral("�.�.�.", "el"));	
		
			//Concept
			model2.add(Ontology.conceptResource, RDFS.subClassOf, OWL.Thing);
		
			//Concept - Announcement Subject
			model2.add(Ontology.announcementSubjectResource, RDFS.subClassOf, Ontology.conceptResource);
			model2.add(Ontology.announcementSubjectResource, RDFS.label, model2.createLiteral("Corporate Announcement Subject", "en"));
			model2.add(Ontology.announcementSubjectResource, RDFS.label, model2.createLiteral("���� ��������� �����������", "el"));
			
			//Concept - Fek Type
			model2.add(Ontology.fekTypeResource, RDFS.subClassOf, Ontology.conceptResource);
			model2.add(Ontology.fekTypeResource, RDFS.label, model2.createLiteral("Fek Type", "en"));
			model2.add(Ontology.fekTypeResource, RDFS.label, model2.createLiteral("����� �.�.�.", "el"));
			
			//conceptScheme
			model2.add(Ontology.conceptSchemeResource, RDFS.subClassOf, OWL.Thing);
		}
			
	}
}