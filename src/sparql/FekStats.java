package sparql;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.vocabulary.ResultSet;
import sparql.QueryConfiguration;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class FekStats {

	public static void main(String[] args) throws IOException {

		ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>();				// list with the fekSubject URIs
		ArrayList<String> fekSubjectsList = new ArrayList<String>();
		ArrayList<String> fekSubjectsUriList = new ArrayList<String>();
		
		System.out.println("Programm starts!");
		
		/* Fek Graph */
		VirtGraph graphFek = new VirtGraph (QueryConfiguration.queryGraphFek, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Fek Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph (QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
			     							  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		System.out.println("fek subjects sparql query starts!");
		results=fekSubjects (graphFek);
		System.out.println("fek subjects sparql query ends!");
		
		// add to details all the subject of feks
		for (int i=0; i<results.size(); i++){
			fekSubjectsList.add(results.get(i).get(1));
			fekSubjectsUriList.add(results.get(i).get(0));
			
			System.out.println(results.get(i).get(1) + "\t" + results.get(i).get(0));
			
		}
		
		//System.out.println("pdf metadata sparql query starts!");
		//pdfMetadata (graphFek, graphOrganizations, fekSubjectsUriList);
		//System.out.println("pdf metadata sparql query ends!");
		
		System.out.println("Newest Corporate Annoncements sparql query starts!");
		newestCorporateAnnoncements (graphFek, graphOrganizations, fekSubjectsList);
		System.out.println("Newest Corporate Annoncements sparql query ends!");
		
	}

	
	/** Get all the fekSubjects of the fek graph **/
    public static ArrayList<ArrayList<String>>  fekSubjects (VirtGraph graphFek) throws IOException{
    	
    	BufferedWriter bufferedWriter = null;						
		String filePath = "C:/Users/Aggelos/workspace/FekProjectRdf/SPARQL/FekSubjects.csv";				// Output file path
		
		ArrayList<ArrayList<String>> resultsList = new ArrayList<ArrayList<String>>();											// list with the fekSubject URIs
		ArrayList<String> details = new ArrayList<String>();
		
		try {
		      
		    bufferedWriter = new BufferedWriter(new FileWriter(filePath));				//Creation of bufferedWriter object
		    String lang = "'el'";
		   		    
		    String queryOrgs = "PREFIX dcterms: <http://purl.org/dc/terms#>\n"+
					"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
					"SELECT DISTINCT (STR(?fekSubjectUri) AS ?fekSubjectUri) (STR(?subjectOfFek) AS ?subjectOfFek)\n"+
					"FROM <http://linkedeconomy.org/FekProject>\n"+
					"WHERE {\n"+
					"?fek dcterms:subject ?fekSubjectUri.\n"+
					"?fekSubjectUri skos:prefLabel ?subjectOfFek.\n"+
					"FILTER(langMatches(lang(?subjectOfFek), "+ lang +"))\n"+
					"}\n"+
					"ORDER BY DESC(?subjectOfFek)";
					 

				VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory. create(queryOrgs, graphFek);		
				com.hp.hpl.jena.query.ResultSet resultsOrgs =  vqeOrgs.execSelect();
				
				while (resultsOrgs.hasNext()) {
					QuerySolution result = (resultsOrgs).nextSolution();
					Literal fekSubjectUri = result.getLiteral("fekSubjectUri");
					Literal subjectOfFek = result.getLiteral("subjectOfFek");
					
					bufferedWriter.write(fekSubjectUri + "," + subjectOfFek);					//Writes the results to the file
					bufferedWriter.write("\n");
					
					details.add(fekSubjectUri.toString());
					details.add(subjectOfFek.toString());
					
					resultsList.add(details);
					
				}
				
				vqeOrgs.close();
				
			}catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		
			finally {
			
			//Closes the BufferedWriter
			try {
				if (bufferedWriter != null) {
					bufferedWriter.flush();
					bufferedWriter.close();
				}
				
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return resultsList;	
    
    }
    
    /** Get all the pdf metadata of the fek graph **/
    public static void  pdfMetadata (VirtGraph graphFek, VirtGraph graphOrganizations, ArrayList<String> SubjectUris) throws IOException{
    	
    	int uriCounter = SubjectUris.size();
    	
		for (int i=0; i<uriCounter; i++){
			
			String fekSubject = SubjectUris.get(i).substring(34);
			String uri = SubjectUris.get(i);
			System.out.print(uri + "\t" + fekSubject + "\n");
		
			BufferedWriter bufferedWriter = null;						
			String filePath = "C:/Users/Aggelos/workspace/FekProjectRdf/SPARQL/" + fekSubject +".csv";				//Output file path
		
			try {
		      
				bufferedWriter = new BufferedWriter(new FileWriter(filePath));				//Creation of bufferedWriter object
				String lang = "'el'";
				String queryOrgs = "PREFIX elod:<http://linkedeconomy.org/ontology#>\n"+
						"PREFIX dcterms:<http://purl.org/dc/terms#>\n"+
						"PREFIX gr:<http://purl.org/goodrelations/v1#>\n"+
						"PREFIX rov:<http://www.w3.org/ns/regorg#>\n"+
						"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
						"SELECT (STR(?organization ) AS ?organization ) (STR(?organizationName) AS ?organizationName) "+ 
						"(STR(?vatId) AS ?vatId) (STR(?subjectOfFek) AS ?subjectOfFek) (STR(?fekNumber) AS ?fekNumber) "+ 
						" (STR(?documentUrl) AS ?documentUrl)\n"+
						"FROM <http://linkedeconomy.org/FekProject>\n"+
						"FROM <http://linkedeconomy.org/Organizations>\n"+
						"WHERE {\n"+
						"?organization gr:vatID ?vatId;"+
									   "rov:orgType ?orgType;\n"+
		    					       "gr:name ?organizationName.\n"+
		    		    "?announcement elod:isAnnouncementOf ?organization;\n"+
		    					       "dcterms:subject <" + uri + ">;\n"+
		    					       "elod:relatedFek ?fek.\n"+
		    			"<" + uri + "> skos:prefLabel ?subjectOfFek.\n"	+
		    			"?fek elod:documentUrl ?documentUrl;"+
		    			"elod:fekNumber ?fekNumber.\n"+
		    			"FILTER(langMatches(lang(?subjectOfFek), "+lang+"))\n"+
		    			"}\n"+
		    			"ORDER BY DESC(?vatId)";
					 
				VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory. create(queryOrgs, graphFek);		
				com.hp.hpl.jena.query.ResultSet resultsOrgs =  vqeOrgs.execSelect();
				
				while (resultsOrgs.hasNext()) {
					QuerySolution result = (resultsOrgs).nextSolution();
					Literal organization = result.getLiteral("organization");
					Literal organizationName = result.getLiteral("organizationName");
					Literal vatId = result.getLiteral("vatId");
					Literal subjectOfFek = result.getLiteral("subjectOfFek");
					Literal fekNumber = result.getLiteral("fekNumber");
					Literal documentUrl = result.getLiteral("documentUrl");
					
					bufferedWriter.write(organization + "," + organizationName + "," + vatId + "," + subjectOfFek + "," + fekNumber + "," + documentUrl);					//Writes the results to the file
					bufferedWriter.write("\n");
				}
				
				vqeOrgs.close();
				
				}catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
		
				finally {
			
					//Closes the BufferedWriter
					try {
						if (bufferedWriter != null) {
							bufferedWriter.flush();
							bufferedWriter.close();
						}
				
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}	
    
		}
    }
    
    
    /** Get all the pdf metadata of the fek graph **/
    public static void  newestCorporateAnnoncements (VirtGraph graphFek, VirtGraph graphOrganizations, ArrayList<String> Subjects) throws IOException{
    	
    	int subjectsCounter = Subjects.size();
    	
		for (int i=0; i<subjectsCounter; i++){
			
			String fekSubject = Subjects.get(i);
			System.out.print(fekSubject + "\n");
		
			BufferedWriter bufferedWriter = null;						
			String filePath = "C:/Users/Aggelos/workspace/FekProjectRdf/SPARQL/" + fekSubject +".csv";				//Output file path
		
			try {
		      
				bufferedWriter = new BufferedWriter(new FileWriter(filePath));				//Creation of bufferedWriter object
				
				String queryOrgs = "PREFIX elod: <http://linkedeconomy.org/ontology#>\n"+
						"PREFIX dcterms:<http://purl.org/dc/terms#>\n"+
						"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n"+
						"SELECT (STR(?organizationName) AS ?organizationName) (STR(?gemh) AS ?gemh) \n"+
						"(STR(?subjectOfFek) AS ?subjectOfFek) (STR(?documentUrl) AS ?documentUrl) ?acceptedDate\n"+
						"FROM <http://linkedeconomy.org/FekProject>\n"+ 
						"FROM <http://linkedeconomy.org/Organizations>\n"+ 
						"WHERE {\n"+
						"?organization elod:fekName ?organizationName;\n"+
									   "elod:gemhNumber ?gemh.\n"+
						"?announcement elod:isAnnouncementOf ?organization;\n"+
									   "dcterms:subject ?fekSubject;"+
									   "elod:relatedFek ?fek.\n"+
		    			"?fekSubject skos:prefLabel ?subjectOfFek.\n"+
		    		    "?fek dcterms:dateAccepted ?acceptedDate;\n"+
		    				  "elod:documentUrl ?documentUrl.\n"+
		    			"FILTER (regex(?subjectOfFek, '^" + fekSubject + "$')) .\n"+
		    			"}\n"	+
		    			"LIMIT 100\n";
					 
				VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory. create(queryOrgs, graphFek);		
				com.hp.hpl.jena.query.ResultSet resultsOrgs =  vqeOrgs.execSelect();
				
				while (resultsOrgs.hasNext()) {
					QuerySolution result = (resultsOrgs).nextSolution();
					Literal organizationName = result.getLiteral("organizationName");
					Literal gemh = result.getLiteral("gemh");
					Literal subjectOfFek = result.getLiteral("subjectOfFek");
					Literal documentUrl = result.getLiteral("documentUrl");
					Literal acceptedDate = result.getLiteral("acceptedDate");
					
					bufferedWriter.write(organizationName + "," + gemh + "," + subjectOfFek + "," + documentUrl + "," + acceptedDate);					//Writes the results to the file
					bufferedWriter.write("\n");
				}
				
				vqeOrgs.close();
				
				}catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
		
				finally {
			
					//Closes the BufferedWriter
					try {
						if (bufferedWriter != null) {
							bufferedWriter.flush();
							bufferedWriter.close();
						}
				
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}	
    
		}
    }
    
    
}