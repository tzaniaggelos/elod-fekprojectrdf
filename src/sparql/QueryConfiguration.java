package sparql;

public class QueryConfiguration {
	
	public static final String queryGraphFek = "http://linkedeconomy.org/FekProject";
	public static final String queryGraphOrganizations = "http://linkedeconomy.org/Organizations";
	
	public static final String connectionString = "jdbc:virtuoso://143.233.226.49:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	//public static final String connectionString = "jdbc:virtuoso://143.233.226.49:8890/autoReconnect=true/charset=UTF-8/log_enable=2";

	public static final String username = "dba";
	public static final String password = "d3ll0lv@69";
	
	//public static final String dateFilterPrevious = "2013";
	//public static final String dateFilterCurrent = "2014";
	
	//public static final String jsonFilepath = "C:/Users/Makis/Desktop/eprocur/JSON/";//"/home/makis/JSON/eprocur/"; "/home/makis/JSON/eprocur/couchDb/";
	//public static final String csvFilepath = "C:/Users/Aggelos/workspace/E_Procurement/CSV/"; //"/home/makis/CSV/";
	//public static final String csvFilepath = "C:/Users/Makis/Desktop/eprocur/CSV/"; //"/home/makis/CSV/";
	
	public static final boolean couchDbUsage = false;

}
