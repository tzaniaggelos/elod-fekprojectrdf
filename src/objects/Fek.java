package objects;


/**
 * @author A. Tzanis
 */


public class Fek {

	private String fekNumber = null;				// Number of the FEK
	private String fekType = null;					// Type of the FEK
	private String originalDate = null;				// Original date of FEK
	private String publDate = null;					// Publication date of FEK
	private String fekSubject = null;				// Subject of the FEK
	private String fekLink = null;					// Download link of the FEK
	private String fekScrapDate = null;				// FEK scrapping date

	public Fek(String fekNumber, String fekType,String originalDate, String publDate, String fekSubject, 
			   String fekLink, String fekScrapDate) {
		
		this.fekNumber = fekNumber;
		this.fekType = fekType;
		this.originalDate = originalDate;
		this.publDate = publDate;
		this.fekSubject = fekSubject;
		this.fekLink = fekLink;
		this.fekScrapDate = fekScrapDate;
			
	}

	public String getFekNumber() {
		return fekNumber;
	}

	public void setFekNumber(String fekNumber) {
		this.fekNumber = fekNumber;
	}


	public String getFekType() {
		return fekType;
	}

	public void setFekType(String fekType) {
		this.fekType = fekType;
	}

	public String getOriginalDate() {
		return originalDate;
	}

	public void setOriginalDate(String originalDate) {
		this.originalDate = originalDate;
	}

	public String getPublDate() {
		return publDate;
	}

	public void setPublDate(String publDate) {
		this.publDate = publDate;
	}

	public String getFekSubject() {
		return fekSubject;
	}

	public void setFekSubject(String fekSubject) {
		this.fekSubject = fekSubject;
	}

	public String getFekLink() {
		return fekLink;
	}

	public void setFekLink(String fekLink) {
		this.fekLink = fekLink;
	}


	public String getFekScrapDate() {
		return fekScrapDate;
	}

	public void setFekScrapDate(String fekScrapDate) {
		this.fekScrapDate = fekScrapDate;
	}
}
