package objects;

import java.util.List;


/**
 * @author A. Tzanis
 */

public class Csv {

	private String afm = null;  					// Company AFM
	private String mae = null;						// Company MAE
	private String gemi = null;						// Company GEMI
	private String companyTitle = null;				// Company title
	private String mainLink = null;					// Company related FEK main link
	private String scrapDate = null;				// Scrapping date of company details
	private List<Fek> fek = null;					// Related to company FEK details list
	private String companyType = null;			    // Csv company type
	

	public Csv(String afm,String mae, String gemi, String companyTitle, String mainLink, String scrapDate, 
			List<Fek> fek, String companyType) {
		
		this.afm = afm;
		this.mae = mae;
		this.gemi = gemi;
		this.companyTitle = companyTitle;
		this.mainLink = mainLink;
		this.scrapDate = scrapDate;
		this.fek = fek;
		this.companyType = companyType;
		
	}

	public String getAfm() {
		return afm;
	}

	public void setAfm(String afm) {
		this.afm = afm;
	}

	public String getMae() {
		return mae;
	}

	public void Mae(String mae) {
		this.mae = mae;
	}

	public String getGemi() {
		return gemi;
	}

	public void setGemi(String gemi) {
		this.gemi = gemi;
	}

	public String getCompanyTitle() {
		return companyTitle;
	}

	public void setCompanyTitle(String companyTitle) {
		this.companyTitle = companyTitle;
	}

	public String getMainLink() {
		return mainLink;
	}

	public void setMainLink(String mainLink) {
		this.mainLink = mainLink;
	}

	public String getScrapDate() {
		return scrapDate;
	}

	public void setScrapDate(String scrapDate) {
		this.scrapDate = scrapDate;
	}


	public List<Fek> getFek() {
		return fek;
	}

	public void setFek(List<Fek> fek) {
		this.fek = fek;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
}