package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.List;

//mport com.hp.hpl.jena.rdf.model.Model;

import actions.CsvActions;
import ontology.Ontology;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * @author A. Tzanis
 */

public class HelperMethods {
	
	
	/**
     * Find the corresponding SKOS Concept, its weight, the English name and the Greek name of the provided type of the criterion.
     * 
     * @param String the type of the criterion
     * @return String the corresponding URI of the SKOS Concept, its weight,English name and Greek name
     * of the provided type of the announcement subject
     */
	public String[] findAnnouncementSubjectIndividual(String fekSubject) {
		
		String[] fekSubjectDtls = null;
		
		if (fekSubject.equalsIgnoreCase("����������� ������������ ��") || fekSubject.equalsIgnoreCase("����������� ������������ ���") || fekSubject.equalsIgnoreCase("������������ ������������ ��") || fekSubject.equalsIgnoreCase("������������ ������������ ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix + "CharterChange", "Charter Change", "����������� ������������"};
		} else if (fekSubject.equalsIgnoreCase("��������� ������� ��������� �������� ��� ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "MonthlyBalanceSheetForBanks", "Monthly Balance Sheet For Banks", "��������� ������� ��������� ��������"};
		} else if (fekSubject.equalsIgnoreCase("����� ��������� ������������ ��������� ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "PermissionForAssetsAquisitions", "Permission For Assets Aquisitions", "����� ��������� ������������ ���������"};
		} else if (fekSubject.equalsIgnoreCase("���������� ����������� �.�. ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "BoardOfDirectorsFormation", "Board Of Directors Formation", "���������� ����������� �.�."};
		} else if (fekSubject.equalsIgnoreCase("������� ��") || fekSubject.equalsIgnoreCase("������� ������������ ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "Formation", "Formation", "�������"};
		} else if (fekSubject.equalsIgnoreCase("��������� - ���") || fekSubject.equalsIgnoreCase("����� ��������� - ��") || fekSubject.equalsIgnoreCase("��������� - ��") || fekSubject.equalsIgnoreCase("������������ - ��") || fekSubject.equalsIgnoreCase("���������� ��") || fekSubject.equalsIgnoreCase("������������ - ���") || fekSubject.equalsIgnoreCase("���������� ���") || fekSubject.equalsIgnoreCase("����� ��������� - ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "OtherCorporateFilings", "Other Corporate Filings", "������������"};
		} else if (fekSubject.equalsIgnoreCase("�������� ��") || fekSubject.equalsIgnoreCase("�������-���� ��������� ��") || fekSubject.equalsIgnoreCase("�������-���� ���") || fekSubject.equalsIgnoreCase("���� ���") ||fekSubject.equalsIgnoreCase("���� ��� ���� �� ���������� - ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "Dissolution", "Dissolution", "���� ��������"};		
		} else if (fekSubject.equalsIgnoreCase("���������� ��������� - ��") || fekSubject.equalsIgnoreCase("���������� ��������� - ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "CorrectionOfErrors", "Correction Of Errors", "���������� ���������"};
		} else if (fekSubject.equalsIgnoreCase("����������� - ��") || fekSubject.equalsIgnoreCase("����������� - ���") || fekSubject.equalsIgnoreCase("������� ���������� ������ ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "BalanceSheet", "Balance Sheet", "�����������"};
		} else if (fekSubject.equalsIgnoreCase("��������� ��") || fekSubject.equalsIgnoreCase("����������� - ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "InvitationToTheShareholders", "Invitation To The Shareholders", "���������"};		
		} else if (fekSubject.equalsIgnoreCase("����������� ��� �� ������ ������� ���") || fekSubject.equalsIgnoreCase("���������� �.�.") || fekSubject.equalsIgnoreCase("���������� ��") || fekSubject.equalsIgnoreCase("���������� ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "Merge", "Merge", "����������"};		
		} else if (fekSubject.equalsIgnoreCase("����������� - ��") || fekSubject.equalsIgnoreCase("����������� - ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "Charter", "Charter", "�����������"};		
		} else if (fekSubject.equalsIgnoreCase("�������� �������� ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "FormationRecall", "Formation Recall", "�������� ��������"};		
		} else if (fekSubject.equalsIgnoreCase("������� ������ ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "AnnouncementOfAFfinancialRight", "Announcement Of A Financial Right", "������� ������"};		
		} else if (fekSubject.equalsIgnoreCase("����� ��������� �������� �����.�����. ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "LicenseToExtentInsuranceServices", "License To Extent Insurance Services", "����� ��������� �������� ������������ ���������"};		
		} else if (fekSubject.equalsIgnoreCase("����������� ��������� ��������� ��")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "InstallationOfForeignCompany", "Installation Of Foreign Company", "����������� ��������� ���������"};		
		} else if (fekSubject.equalsIgnoreCase("������ ��������� �������.��������� ���")) {
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "NetWorthAssessment", "Net Worth Assessment", "�������� ������� �����"};		
		} else { 
			fekSubjectDtls = new String[]{Ontology.eLodPrefix  + "Unknown", "", ""};
			writeUnknownMetadata("fekSubject", fekSubject);
		}
		
		return fekSubjectDtls;
	}
	
	
	/**
     * Find the corresponding SKOS Concept, its weight, the English name and the Greek name of the provided type of the criterion.
     * 
     * @param String the type of the criterion
     * @return String the corresponding URI of the SKOS Concept, its weight,English name and Greek name
     * of the provided type of the organization category
     */
	public String[] findOrganizationCategoryIndividual(String organizationCategory) {
		
		String[] organizationCategoryDtls = null;
		
	if (organizationCategory.equalsIgnoreCase("AE")) {
		organizationCategoryDtls = new String[]{Ontology.instancePrefix + "OrganizationCategory/AE", "SA (corpotation)", "������� ��������"};		
	} else if (organizationCategory.equalsIgnoreCase("EPE")) {
		organizationCategoryDtls = new String[]{Ontology.instancePrefix + "OrganizationCategory/EPE", "Private limited company", "�������� ������������� �������"};		
	}else { 
		organizationCategoryDtls = new String[]{Ontology.instancePrefix  + "Unknown", "", ""};
		writeUnknownMetadata("organizationCategory", organizationCategory);
	}
	
		return organizationCategoryDtls;
	}
	
	
	/**
     * Find the corresponding SKOS Concept, its weight, the English name and the Greek name of the provided type of the criterion.
     * 
     * @param String the type of the criterion
     * @return String the corresponding URI of the SKOS Concept, its weight,English name and Greek name
     * of the provided type of the fek type
     */
	public String[] findFekTypeIndividual(String fekType) {
		
		String[] fekTypeDtls = null;
		
	if (fekType.equalsIgnoreCase("��-���")) {
		fekTypeDtls = new String[]{Ontology.instancePrefix + "FekType/Ae-Epe", "��-���"};		
	} else if (fekType.equalsIgnoreCase("���.�.�.�.")) {
		fekTypeDtls = new String[]{Ontology.instancePrefix + "FekType/Pradit", "���.�.�.�."};		
	}else { 
		fekTypeDtls = new String[]{Ontology.instancePrefix  + "Unknown", ""};
		writeUnknownMetadata("fekType", fekType);
	}
	
		return fekTypeDtls;
	}
	
	
	/**
     * Find the filenames of the directories containing the CSV files.
     * 
     * @param String the path of the directory where the directories exist
     * @return List<String> the filenames of the directories containing the CSV files
     */
	public List<String> getDirectoryNames(String filePath) {
		
		List<String> namesList = new ArrayList<String>();
		File[] files = new File(filePath).listFiles();

		for (File file : files) {
		    if (file.isDirectory()) {
		        namesList.add(file.getName());
		    }
		}
		
		return namesList;
	}
	
	/**
     * Find the the filenames of the CSV of the provided directory.
     * 
     * @param String the path of the directory where the files exist
     * @return List<String> the filenames of the CSV
     */
	public List<String> getCsvFileNames(String filePath) {
		
		List<String> namesList = new ArrayList<String>();
		File[] files = new File(filePath).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		        namesList.add(file.getName());
		    }
		}
		
		return namesList;
	}
	
	
	/**
     * Move the processed CSV files to a new directory.
     * 
     * @param String the directory name of the CSV file
     * @param String the date the process was initiated 
     * @param String the name of the CSV file
     */
	public void moveProcessedCsvFiles(String directoryName, String processDate, String csvFileName) {
		
		File csvFile = new File(CsvActions.filePathCsv + directoryName + "/" + csvFileName);
		Path from = csvFile.toPath();
		File toFile = directoryCreator (CsvActions.filePathCsvProcessed, directoryName + "/" + processDate);		// Create AE_pdf directory
		Path to = Paths.get(toFile + "/" + csvFile.getName());
		writeUnknownMetadata("processedCsvFilenames", csvFileName);													// Write the processed folders
		try {
			Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 *  Create a new directory.
	 *  
	 *  @param String the directory Path in which we want to create new directory
	 *  @param String the directory Name we want the created directory to have
	 */
    public static File directoryCreator (String directoryPath, String directoryName) {
       
    	File newDirectory = new File(directoryPath, directoryName);						// Create a new directory
		if(!newDirectory.exists()){
			newDirectory.mkdirs();
		}
		
        return newDirectory;
    }
  	
    
    /**
     * Read the data from the text file and return them in a list.
     * 
     * @return List<String> the data of the text file
     */
	public List<String> readProcessedCsvFiles() {
		
		List<String> processedCsvList = new ArrayList<String>();
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(CsvActions.filePathMain + "processedCsvFilenames.txt"));
			
			String line;
			while ((line = in.readLine()) != null) {
				processedCsvList.add(line);
			}
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot read processed CSV file!");
		}

		return processedCsvList;
	}
	
	
	/**
     * Find and transform the current date into the dd-MM-yyyy format.
     * 
     * @return String the current date in the dd-MM-yyyy format
     */
	public String getCurrentDate() {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = sdf.format(cal.getTime());
		
		return currentDate;
	}
	
	
	/**
     * Export to a file the unknown metadata.
     * 
     * @param String the output filename
     * @param String the unknown metadata
     */
	public void writeUnknownMetadata(String fileName, String metadata) {
		
		try {
		    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName + ".txt", true), "UTF8"));
		    out.append(metadata);
		    out.append(System.getProperty("line.separator"));
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	
	
	
}