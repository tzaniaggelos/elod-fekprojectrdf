package actions;

import java.util.Arrays;
import java.util.List;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import ontology.Ontology;
import utils.HelperMethods;

/**
 * @author A. Tzanis
 **/

public class ConceptCreator {

	
private HelperMethods hm = new HelperMethods();
	
	/**
     * Add to the model the Announcement Subjects.
     * 
     * @param Model the model we are currently working with
     */
	public void addAnnouncementSubjectToModel(Model model) {
		
		List<String> announcementSubjectList = Arrays.asList("����������� ������������ ��", "��������� ������� ��������� �������� ��� ��", "����� ��������� ������������ ��������� ��", "���������� ����������� �.�. ��", "����������� ������������ ���",
												"������������ ������������ ��", "������������ ������������ ���" , "����� ��������� - ��" , "��������� - ��" , "������������ - ��" , "���������� ��" , "���������� ���" ,
												"�������� ��" , "���������� ��������� - ��" , "���������� ��������� - ���" , "����������� - ��", "����������� - ���", "��������� ��", "����������� - ��", "���������� �.�.", 
												"���������� ��", "������� ��", "����������� - ��", "������� ���������� ������ ��", "�������� �������� ��", "������� ������ ��", "�������-���� ��������� ��", "������� ������������ ���",
												"����� ��������� �������� �����.�����. ��", "����������� ��������� ��������� ��", "���������� ���", "����� ��������� - ���","����� ��������� - ���","���� ��� ���� �� ���������� - ���",
												"���� ���","�������-���� ���","������ ��������� �������.��������� ���","������������ - ���","����������� - ���","��������� - ���","����������� ��� �� ������ ������� ���");
		
		for (String announcementSubject : announcementSubjectList) {
			
			String[] announcementSubjectDtls = hm.findAnnouncementSubjectIndividual(announcementSubject);
			
			/** Create concept Resource **/
			Resource conceptResource = model.createResource(announcementSubjectDtls[0], Ontology.announcementSubjectResource);
			model.createResource(announcementSubjectDtls[0], Ontology.conceptResource);
			
			/** configure concept prefLabel **/
			conceptResource.addProperty(Ontology.prefLabel, announcementSubjectDtls[2], "el");
			conceptResource.addProperty(Ontology.prefLabel, announcementSubjectDtls[1], "en");
			
			/** configure concept properties **/
			conceptResource.addProperty(Ontology.inScheme, Ontology.announcementSubjectSchemeResource);
			conceptResource.addProperty(Ontology.topConceptOf, Ontology.announcementSubjectSchemeResource);
			
			/** Create conceptScheme Resource **/
			Resource conceptSchemeResource = model.createResource(Ontology.eLodPrefix + "AnnouncementSubjectScheme", Ontology.conceptSchemeResource);
			
			/** configure conceptScheme property **/
			conceptSchemeResource.addProperty(Ontology.hasTopConcept, conceptResource);
			
		}
		
	}
	
	/**
     * Add to the model the Organization Categories.
     * 
     * @param Model the model we are currently working with
     */
	public void addOrganizationCategoryToModel(Model model) {
		
		List<String> organizationCategoryList = Arrays.asList("AE", "EPE");
		
		for (String organizationCategory : organizationCategoryList) {
			
			String[] organizationCategoryDtls = hm.findOrganizationCategoryIndividual(organizationCategory);
			
			/** Create concept **/
			Resource organizationCategoryResource = model.createResource(organizationCategoryDtls[0], Ontology.organizationCategoryResource);
			model.createResource(organizationCategoryDtls[0], Ontology.conceptResource);
			
			/** configure concept prefLabel **/
			organizationCategoryResource.addProperty(Ontology.prefLabel, organizationCategoryDtls[2], "el");
			organizationCategoryResource.addProperty(Ontology.prefLabel, organizationCategoryDtls[1], "en");
			
		}
	}
	
	/**
     * Add to the model the Fek Categories.
     * 
     * @param Model the model we are currently working with
     */
	public void addFekCategoryToModel(Model model) {
		
		List<String> fekTypeList = Arrays.asList("��-���", "���.�.�.�.");
		
		for (String fekType : fekTypeList) {
			
			String[] fekTypeDtls = hm.findFekTypeIndividual(fekType);
			
			/** Create concept **/
			Resource fekTypeResource = model.createResource(fekTypeDtls[0], Ontology.fekTypeResource);
			model.createResource(fekTypeDtls[0], Ontology.conceptResource);
			
			/** configure concept prefLabel **/
			fekTypeResource.addProperty(Ontology.prefLabel, fekTypeDtls[1], "el");
			
		}
	}
}
