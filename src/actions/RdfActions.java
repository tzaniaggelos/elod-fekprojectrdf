package actions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import objects.Csv;
import objects.Fek;
import ontology.Ontology;
import ontology.OntologyInitialization;
import utils.HelperMethods;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;


/**
 * @author A. Tzanis
 */

public class RdfActions {
	
	/**
     * Fetch all the details from the provided Csv object, 
     * create the RDF triples and add them to the existing model.
     * 
     * @param csvObject a Csv file object
     * @param Model the models to add the triples
     */
	public void createRdfFromCsv(Csv csvObject, Model model1, Model model2) {
		
		HelperMethods hm = new HelperMethods();
		String fekType = null;
		
	/** GeneralRdf **/
		
		/** organizationCategoryResource **/
		Resource organizationCategoryResource = null;
		String[] organizationCategoryIndividualUri = hm.findOrganizationCategoryIndividual(csvObject.getCompanyType());
		//String[] organizationCategoryIndividualUri = hm.findOrganizationCategoryIndividual("AE");
		organizationCategoryResource = model1.createResource(organizationCategoryIndividualUri[0], Ontology.organizationCategoryResource);
		
		/** agentResource **/
		Resource agentResource = null;
		if (csvObject.getAfm() != null && !csvObject.getAfm().isEmpty()) {										
			agentResource = model1.createResource(Ontology.instancePrefix + "Organization/" + csvObject.getAfm().replaceAll("\\s+",""), Ontology.organizationResource);
		model1.createResource(Ontology.instancePrefix + "Organization/" + csvObject.getAfm().replaceAll("\\s+",""), Ontology.businessEntityResource);
		model1.createResource(Ontology.instancePrefix + "Organization/" + csvObject.getAfm().replaceAll("\\s+",""), Ontology.orgOrganizationResource);
		model1.createResource(Ontology.instancePrefix + "Organization/" + csvObject.getAfm().replaceAll("\\s+",""), Ontology.registeredOrganizationResource);
		}
		// Company maeNumber
		if (csvObject.getMae() != null && !csvObject.getMae().isEmpty()) {										
			agentResource.addLiteral(Ontology.maeNumber, model1.createTypedLiteral(csvObject.getMae().replaceAll("\\s+",""), XSDDatatype.XSDstring));		
    	}
		// Company gemiNumber
		if (csvObject.getGemi() != null && !csvObject.getGemi().isEmpty()) {										
			agentResource.addLiteral(Ontology.gemhNumber, model1.createTypedLiteral(csvObject.getGemi().replaceAll("\\s+",""), XSDDatatype.XSDstring));		
		 }
		// Company etUrl
		if (csvObject.getMainLink() != null && !csvObject.getMainLink().isEmpty()) {										
			agentResource.addLiteral(Ontology.etUrl, model1.createTypedLiteral(csvObject.getMainLink().replaceAll("\\s+",""), XSDDatatype.XSDstring));		
		}
		// Company fekName
		if (csvObject.getCompanyTitle() != null && !csvObject.getCompanyTitle().isEmpty()) {										
			agentResource.addLiteral(Ontology.fekName, model1.createTypedLiteral(csvObject.getCompanyTitle(), XSDDatatype.XSDstring));		
		}
		// Company modified
		if (csvObject.getScrapDate() != null && !csvObject.getScrapDate().isEmpty()) {										
			agentResource.addLiteral(Ontology.modified, model1.createTypedLiteral(csvObject.getScrapDate(), XSDDatatype.XSDdate));
		}
		// Company orgType
		if (organizationCategoryResource != null){
			agentResource.addProperty(Ontology.orgType, organizationCategoryResource);
		}
		
	/** DecisionsRdf **/
		
		
		if ( (csvObject.getFek() != null && !csvObject.getFek().isEmpty()) ) {
			
			int fekCounter = csvObject.getFek().size();		// Stores the number of related fek each company has
			
			System.out.println("\n Fek counter is: "+fekCounter+"\n");
    		
			for (int i=0; i<fekCounter; i++){
    			
    			Fek relatedFek = csvObject.getFek().get(i);		// Current fek
    			
    			/** FekSubjectResource **/
    			Resource fekSubjectResource = null;
    			if (relatedFek.getFekSubject() != null && !relatedFek.getFekSubject().isEmpty()) {
    				String[] subjectIndividualUri = hm.findAnnouncementSubjectIndividual(relatedFek.getFekSubject());
    				fekSubjectResource = model2.createResource(subjectIndividualUri[0], Ontology.conceptResource);
    			}
    			
    			/** FekTypeResource **/
    			Resource fekTypeResource = null;
    			if (relatedFek.getFekType() != null && !relatedFek.getFekType().isEmpty()) {
    				String[] fekTypeIndividualUri = hm.findFekTypeIndividual(relatedFek.getFekType());
    				fekTypeResource = model2.createResource(fekTypeIndividualUri[0], Ontology.fekTypeResource);
    			}
    			
    			/** FekResource **/
    			Resource fekResource = null;
    			if ((relatedFek.getFekType() != null && relatedFek.getOriginalDate() != null && relatedFek.getFekNumber() != null)) {
    				
    				if (relatedFek.getFekType().equalsIgnoreCase("��-���")){
    					fekType = "fektype_AEEPE";
    				}
    				else if (relatedFek.getFekType().equalsIgnoreCase("���.�.�.�.")){
    					fekType = "fektype_PRADIT";
    				}
    				
    				fekResource = model2.createResource(Ontology.instancePrefix + "Fek/" + fekType + "/" + relatedFek.getOriginalDate().substring(0, 4) + "/" + relatedFek.getFekNumber(), Ontology.fekResource);
    			
    			}
    			// Fek fekType
    			if (fekTypeResource != null) {
    				fekResource.addProperty(Ontology.fekType, fekTypeResource);
    			}
    			// Fek fekYear
    			if ( (relatedFek.getOriginalDate() != null && !relatedFek.getOriginalDate().isEmpty()) ) {
    				fekResource.addProperty(Ontology.fekYear, model2.createTypedLiteral(relatedFek.getOriginalDate().substring(0, 4), XSDDatatype.XSDgYear));
    			}
    			// Fek fekNumber
    			if (relatedFek.getFekNumber() != null && !relatedFek.getFekNumber().isEmpty())  {
    				fekResource.addProperty(Ontology.fekNumber, model2.createTypedLiteral(relatedFek.getFekNumber(), XSDDatatype.XSDstring));
    			}
    			// Fek issued
    			if (relatedFek.getPublDate() != null && !relatedFek.getPublDate().isEmpty()) {										
    				fekResource.addLiteral(Ontology.issued, model2.createTypedLiteral(relatedFek.getPublDate(), XSDDatatype.XSDdate));
    			} 
    			// Fek dateAccepted
    			if (relatedFek.getOriginalDate() != null && !relatedFek.getOriginalDate().isEmpty()) {										
    				fekResource.addLiteral(Ontology.dateAccepted, model2.createTypedLiteral(relatedFek.getOriginalDate(), XSDDatatype.XSDdate));
    			}
    			// Fek documentUrl
    			if (relatedFek.getFekLink() != null && !relatedFek.getFekLink().isEmpty()) {										
    				fekResource.addLiteral(Ontology.documentUrl, model2.createTypedLiteral(relatedFek.getFekLink(), XSDDatatype.XSDstring));
    			}
    			
    			/** CorporateAnnouncementResource **/
    			Resource corporateAnnouncementResource = null;
    			if ( (csvObject.getAfm() != null && relatedFek.getFekNumber() != null && relatedFek.getOriginalDate() != null) ) {
    				corporateAnnouncementResource = model2.createResource(Ontology.instancePrefix + "CorporateAnnouncement/" + csvObject.getAfm().replaceAll("\\s+","") + "/" + relatedFek.getFekNumber().replaceAll("\\s+","") + "/" + relatedFek.getOriginalDate().replaceAll("\\s+",""), Ontology.corporateAnnouncementResource);
    			}
    			// Decision isAnnouncementOf
    			if (csvObject.getAfm() != null && !csvObject.getAfm().isEmpty()) {
    				corporateAnnouncementResource.addProperty(Ontology.isAnnouncementOf, model2.createResource(Ontology.instancePrefix + "Organization/" + csvObject.getAfm().replaceAll("\\s+","")));
    			}
    			// Decision relatedFek
    			if (fekResource != null) {
    				corporateAnnouncementResource.addProperty(Ontology.relatedFek, fekResource);
    			}
    			// Decisions subject 
    			if (fekSubjectResource != null) {										
    				corporateAnnouncementResource.addProperty(Ontology.subject, fekSubjectResource);
    			}
    			// Decisions modified 
    			if (relatedFek.getFekScrapDate() != null && !relatedFek.getFekScrapDate().isEmpty()) {										
    				corporateAnnouncementResource.addLiteral(Ontology.modified, model2.createTypedLiteral(relatedFek.getFekScrapDate(), XSDDatatype.XSDdate));
    			}
    		}
		}
	}
	
	/**
     * Fetch the existing model.
     * 
     * @param i is the model number, j is the rdf name number
     * @return Model the new model
     */
	public Model Model (int i, int j) {
		
		OntologyInitialization ontInit = new OntologyInitialization();
		Model Model1=null;
		Model Model2=null;
		
		if (i==1){
			
			Model1 = ModelFactory.createDefaultModel();
			
			try {
				InputStream is = new FileInputStream(CsvActions.filePathOutput + "/GeneralCsvEpe_Part_" + j + ".rdf");
				Model1.read(is,null);
				is.close();
			} catch (Exception e) { //empty file
			}
			
			//Perform the basic initialization on the model1 and on the model2
			ontInit.setPrefixes(Model1, Model2);
			ontInit.createHierarchies(Model1, Model2);
			return Model1;
		}
		
		else {
			
			Model2 = ModelFactory.createDefaultModel();
			
			try {
				InputStream is = new FileInputStream(CsvActions.filePathOutput + "/DesisionCsv_Part_" + j + ".rdf");
				Model2.read(is,null);
				is.close();
			} catch (Exception e) { //empty file
			}
			
			//Perform the basic initialization on the model1 and on the model2
			ontInit.setPrefixes(Model1, Model2);
			ontInit.createHierarchies(Model1, Model2);
			return Model2;
			}
	}
	
	
	/**
     * Store the Model.
     * 
     * @param Model the model
     */
	public void writeModel(Model model1, Model model2, int i) {
		
		try {
			System.out.println("\nSaving Model1...");
			FileOutputStream fos1 = new FileOutputStream(CsvActions.filePathOutput + "/GeneralCsv_Part_" + i + ".rdf");
			model1.write(fos1, "RDF/XML-ABBREV", CsvActions.filePathOutput + i);
			fos1.close();
			
			System.out.println("\nSaving Model2...");
			FileOutputStream fos2 = new FileOutputStream(CsvActions.filePathOutput + "/DecisionsCsv_Part_" + i + ".rdf");
			model2.write(fos2, "RDF/XML-ABBREV", CsvActions.filePathOutput + i);
			fos2.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}