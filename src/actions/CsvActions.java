package actions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import au.com.bytecode.opencsv.CSVReader;
import objects.Csv;
import objects.Fek;
import utils.HelperMethods;

/**
 * @author A. Tzanis
 **/

public class CsvActions {
	
	// Local
	/*public static final String filePathCsv = "C:/Users/Aggelos/workspace/TheFekProject/";							// Csv files filePath
	public static final String filePathOutput = "C:/Users/Aggelos/workspace/FekProjectRdf/RdfFiles";				// RDF'S output filePath
	public static final String filePathCsvProcessed = "C:/Users/Aggelos/workspace/FekProjectRdf/ProcessedCsv/"; 	// Processed csv files filePath
	public static final String filePathMain = "C:/Users/Aggelos/workspace/FekProjectRdf/";							// Main workspace filepath
	*/
	
	// Server
	public static final String filePathCsv = "/home/tzanis/theFekProject/fekHarvesting/";							// Csv files filePath
	public static final String filePathOutput = "/home/tzanis/theFekProject/fekRdfizing/RdfFiles";				// RDF'S output filePath
	public static final String filePathCsvProcessed = "/home/tzanis/theFekProject/fekRdfizing/ProcessedCsv/"; 	// Processed csv files filePath
	public static final String filePathMain = "/home/tzanis/theFekProject/fekRdfizing/";							// Main workspace filepath
	
	
	public Csv readCsv(String csvFilePath, String directoryName) {
		
		HelperMethods hm = new HelperMethods();
				
		int fekCounter = 0;							// Count the number of the related fek's 
		int corrector = 0;							// Finds the posistion of the main link. We need to correct errors from the csv's
		int difference = 0;							// Finds the difference of the write possistion
		int line = 0;								// Stores the current line of csv
		CSVReader reader = null;
		String details = null;
		
		// CSV details
		String afm = null;  						// Company AFM
		String mae = null;							// Company MAE
		String gemi = null;							// Company GEMI
		String companyTitle = null;					// Company title
		String mainLink = null;						// Main link to the company related fek page
		String scrapDate = null;					// Scrapping date of company details
		String companyType = directoryName;
		
		// Fek details
		String fekNumber = null;					// FEk number
		String fekType = null;						// Tyoe of fek 
		String originalDate = null;					// Original date of fek
		String publDate = null;						// Publication date of fek
		String fekSubject = null;					// Subject of the fek		
		String fekLink = null;						// The download link of the fek
		String fekScrapDate = null;					// Scrapping date of the fek

		List<Fek> fekList = new ArrayList<Fek>();
		
		Fek fekObject = null;
		Csv csvObject = null;
		
		String[] fields;
		
		try {
			
			if (companyType.equalsIgnoreCase("AE") || companyType.equalsIgnoreCase("EPE")){
				reader = new CSVReader(new InputStreamReader(new FileInputStream(csvFilePath), "UTF-8"), 
															 ',', '"', '\0');
			}
			else {
				reader = new CSVReader(new FileReader(csvFilePath), ',', '"', '\0');				// In case directory is searchResultsFinal
				companyType = "AE";
			}
			
			String[] nextLine;
			
			try {
				
				System.out.println("\n" + csvFilePath);
				
				while ((nextLine = reader.readNext()) != null) {
					
					fields = new String[nextLine.length];
					line++;												// Increase the line number by one 
					
					/** Csv details **/
					if ( line==1 ){										// Check if the link contain the company details
					
						corrector = 0;
						
						/** AE company **/
						if (companyType.equalsIgnoreCase("AE") ) {			// Case company type is AE
							
							for (int i = 0; i < nextLine.length; i++) {
							
								fields[i] = nextLine[i];
							
								if (fields[i].startsWith("http")){			// Finds the real posistion of main Link. It must be 4, but sometimes it is not.
									corrector = i;							// The reason is that the company title in the csv in some cases contains "," 		
								}											// to separate the multiple company titles (����������, ������� or Real 98,6 fm)
							}
						
							// Makes the needed changes to the csv 
							if (corrector >=4) {
							
								difference = corrector-4;					// Finds the difference of the correct possision to the real possistion (6-4=2)
								String tmp = fields[3];						// Stores the field[3] string to the temp
							
								for (int j=4; j<corrector; j++){
									tmp = tmp + "," + fields[j];
								}
								fields[3] = tmp;							// At the end stores the temp to the fields[3]
							}
						
							afm = fields[0];
							System.out.print(afm+"\t");
							mae = fields[1];
							System.out.print(mae+"\t");
							gemi = fields[2];
							System.out.print(gemi+"\t");
							companyTitle = fields[3];
							System.out.print(companyTitle+"\t");
							mainLink = fields[4+difference];						// The real possision is the correct possision (4) plus the difference (if we have)
							System.out.print(mainLink+"\t");
							scrapDate = fields[5+difference].substring(0, 10);		// The real possision is the correct possision (5) plus the difference (if we have)
							System.out.print(scrapDate+"\t");
						
						}
						/** EPE company **/
						else if (companyType.equalsIgnoreCase("EPE")) {				// Case company type is EPE
							
							for (int i = 0; i < nextLine.length; i++) {
								
								fields[i] = nextLine[i];
							
								if (fields[i].startsWith("http")){			// Finds the real posistion of main Link. It must be 4, but sometimes it is not.
									corrector = i;							// The reason is that the company title in the csv in some cases contains "," 		
								}											// to separate the multiple company titles (����������, ������� or Real 98,6 fm)
							}
						
							// Makes the needed changes to the csv 
							if (corrector >=3) {
							
								difference = corrector-3;					// Finds the difference of the correct possision to the real possistion (5-3=2)
								String tmp = fields[2];						// Stores the field[2] string to the temp
							
								for (int j=3; j<corrector; j++){
									tmp = tmp + "," + fields[j];
								}
								fields[2] = tmp;							// At the end stores the temp to the fields[2]
							}
							
							afm = fields[0];
							System.out.print(afm+"\t");
							if (!fields[1].contains("����:")){				// In case we have a gemi number given from harvesting
								gemi = fields[1];
								System.out.print(gemi+"\t");
							}
							companyTitle = fields[2];
							System.out.print(companyTitle+"\t");
							mainLink = fields[3+difference];						// The real possision is the correct possision (3) plus the difference (if we have)
							System.out.print(mainLink+"\t");
							scrapDate = fields[4+difference].substring(0, 10);		// The real possision is the correct possision (4) plus the difference (if we have)
							System.out.print(scrapDate+"\t");
						}
						
					}
					
					/** decision details  **/
					else if (line>1){											// Check if line contain the related fek details
						
						System.out.print("\n");
						
						for (int i = 0; i < 7; i++) {
							fields[i] = nextLine[i];
						}
						
						if (fekCounter>0){
							// Create a fek object
							fekObject = new Fek(fekNumber, fekType, originalDate, publDate, fekSubject, fekLink, fekScrapDate);
						
							fekList.add(fekObject); 	//more than 1 fek
						
							// Reset fek Strings for next usage
							fekNumber = null;
							fekType = null;
							originalDate = null;
							publDate = null;
							fekSubject = null;
							fekLink = null;
							fekScrapDate = null;
						}
						
						// Continue with storing fek Strings
						fekNumber = fields[0];
						System.out.print(fekNumber+"\t");
						fekType = fields[1];
						System.out.print(fekType+"\t");
						originalDate = fields[2];
						System.out.print(originalDate+"\t");
						if (!fields[3].equalsIgnoreCase("null")){
							publDate = fields[3];
							System.out.print(publDate+"\t");
						}
						fekSubject = fields[4];
						System.out.print(fekSubject+"\t");
						fekLink = fields[5];
						System.out.print(fekLink+"\t");
						fekScrapDate = fields[6];
						System.out.print(fekScrapDate+"\t");
						
						fekCounter++;
						
						if (fekSubject.equalsIgnoreCase("����������� - ���")){
							details = fekSubject + "," + fekLink;
							hm.writeUnknownMetadata("UnknownSubjects", details);
						}
						
						
					}
					
					
				}
				
				reader.close();
				
				if (line > 1) {						// Create the unique or the last fek object IF there is a fek object								
					
					
					fekObject = new Fek(fekNumber, fekType, originalDate, publDate, fekSubject, fekLink, fekScrapDate);

					fekList.add(fekObject);
					
				}			
				
				// Create object type of CSV
				csvObject = new Csv(afm, mae, gemi, companyTitle, mainLink, scrapDate, fekList, companyType);
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return csvObject;
	}
}