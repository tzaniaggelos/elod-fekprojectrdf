package actions;

import java.util.ArrayList;
import java.util.List;
import objects.Csv;
import com.hp.hpl.jena.rdf.model.Model;
import utils.HelperMethods;

/**
 * @author A. Tzanis
 */

public class Main {
	
		
	/**
     * Method to initiate the process
     */
	public static void main(String[] args) {
		
		boolean createConcepts = true;
		
		HelperMethods hm = new HelperMethods();
		CsvActions csvMethods = new CsvActions();
		RdfActions rdfActions = new RdfActions();
		ConceptCreator concept = new ConceptCreator();
		int counter = 0;													// Count the amount of csv
		int part = 0;														// The part of the rdf file
		
		List<String> csvFileNameList = new ArrayList<String>();				// List of csv file names
		List<String> direcoryNameList = new ArrayList<String>();			// List of directory names
		direcoryNameList = hm.getDirectoryNames(CsvActions.filePathCsv);	// Stores the filePath directory direstory names to the list
		List<String> processedCsvList = new ArrayList<String>();			// List of processed Csv files
		String processDate = null;											// String to store the process date
		
		// Create the models
		Model model1 = rdfActions.Model(1,0); 
		Model model2 = rdfActions.Model(2,0); 
		
		if (createConcepts) {
			concept.addOrganizationCategoryToModel(model1);					// Add the Organization Categories to model1
			concept.addAnnouncementSubjectToModel(model2);					// Add the Announcement Subjects to model2
			concept.addFekCategoryToModel(model2);							// Add the Fek Categories to model2
		}
		
		processedCsvList = hm.readProcessedCsvFiles();						// Load the processed CSV files
		processDate = hm.getCurrentDate();									// Current date
		
		for (String directoryName : direcoryNameList) {
			
			if ( directoryName.equalsIgnoreCase("AE") || directoryName.equalsIgnoreCase("EPE") ) {
				
				csvFileNameList = hm.getCsvFileNames(CsvActions.filePathCsv + directoryName);
				
				for (String csvFileName : csvFileNameList) {
					
					if (!processedCsvList.contains(csvFileName)) { //file not in list
						
						counter++;
					
						Csv csvObject = csvMethods.readCsv(CsvActions.filePathCsv + directoryName + "/" + csvFileName, directoryName);
						//Csv csvObject = csvMethods.readCsv(CsvActions.filePathCsv + "094407383.csv", "094407383.csv");//csvFileName);
						rdfActions.createRdfFromCsv(csvObject, model1, model2);
						hm.moveProcessedCsvFiles(directoryName, processDate, csvFileName); 		// Move the file to Processed file
						
						if (counter%10000 == 0){
						
							part++;
							// Save the models
							rdfActions.writeModel(model1, model2, part);				
							// Close the models
							model1.close();														// close current model1
							model2.close();														// close current model2
							// Recreate the models
							model1 = rdfActions.Model(1,part+1); 								// create the new model1
							model2 = rdfActions.Model(2,part+1);								// create the new model2
						
							if (createConcepts) {												// check if there are concepts to add
								concept.addOrganizationCategoryToModel(model1);					// Add the Organization Categories to model1
								concept.addAnnouncementSubjectToModel(model2);					// Add the Announcement Subjects to model2
								concept.addFekCategoryToModel(model2);							// Add the Fek Categories to model2
							}
						}
					}
				}
				
				hm.writeUnknownMetadata("foldersOK", directoryName);							// Write the processed folders
				
			}
			
			processedCsvList = hm.readProcessedCsvFiles();										// Load the processed CSV files at the and of each folder
			
		}
				
		rdfActions.writeModel(model1, model2, part+1);											// Write the last models
		model1.close();																			// close the last model1
		model2.close();																			// close the last model2
		
		System.out.println("\n Number of csv's processed: " + counter);							// Print the number of csv's have not been processed
		
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append(counter);
		String strI = sb.toString();
		
		hm.writeUnknownMetadata("numberOfProcessedCsv", strI);	
	}
}